from django.shortcuts import render
from .forms import *
from .models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


def index(request):
    return render(request, 'Afrahsapp/myWbsite.html')

def about(request):
    return render(request, 'Afrahsapp/about.html')

def contact(request):
    return render(request, 'Afrahsapp/contact.html')

def photos(request):
    return render(request, 'Afrahsapp/photos.html')

def form(request):
    form = Subscribe_Form()
    return render(request, 'Afrahsapp/form.html', {"form" : form})


@csrf_exempt
def validate(request):
    email = request.POST.get('email')
    data = {'not_valid': Subscribe.objects.filter(email=email).exists()}
    return JsonResponse(data)

def success(request):
    submited_form = Subscribe_Form(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscribe(email=cd['email'], name=cd['name'], password=cd['password'])
        new_subscriber.save()
        data = {'email': cd['email'],'name': cd['name'], 'password': cd['password'], }
    return JsonResponse(data)
