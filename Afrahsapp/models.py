from django.db import models

# Create your models here.

class Subscribe(models.Model):
    email = models.EmailField()
    name = models.TextField(max_length=100)
    password = models.TextField()
