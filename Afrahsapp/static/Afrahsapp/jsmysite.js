$(document).ready(function() {
  var name = "";
  var password = "";
  $("#id_name").change(function() {
        name = $(this).val();
  });
  $("#id_password").change(function() {
        password = $(this).val();
  });

  $("#id_email").change(function(){
        var email = $(this).val();
        console.log(email);
        console.log(name);
        console.log(password);
        $.ajax({
          url: "/validate",
          data: {
                'email': email,
          },
          method: "POST",
          dataType: 'json',
          success: function(data){
            console.log("MASUK");
            if(data.not_valid){
              alert("This email is already taken");
            }
            else{
              if(name.length > 0 && password.length > 5){
                console.log("NYAMPE");
                $("#submit").removeAttr("disabled");
              }
            }
          }
  });
});

$("#submit").click(function() {
  event.preventDefault();
  $.ajax({
    headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
    url:"/success",
    data: $("form").serialize(),
    method: 'POST',
    dataType: 'json',
    success: function(data){
      alert("Thank you for subscribing!");
    },
    error: function(data){
      alert("Something went wrong");
    }
  });
});
});
