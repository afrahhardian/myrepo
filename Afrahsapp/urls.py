from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name = "index"),
    path('myWbsite/', views.index, name ="myWbsite"),
    path('about/', views.about, name = "about"),
    path('contact/', views.contact, name = "contact"),
    path('photos/',views.photos, name = "photos"),
    path('form/',views.form, name = "form"),
    path('validate',views.validate, name = 'validate'),
    path('success',views.success, name = 'sucess'),
]
