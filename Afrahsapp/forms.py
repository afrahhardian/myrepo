from django import forms

class Subscribe_Form(forms.Form):
    name = forms.CharField(label = "Name", required = True, max_length = 100, widget=forms.TextInput(attrs= {'class' : 'form-control'}))
    password = forms.CharField(label = "Password", required = True, min_length = 8, widget = forms.PasswordInput(attrs = {'class' : 'form-control'}))
    email = forms.EmailField(label = "Email", required = True, widget = forms.EmailInput(attrs = {'class' : 'form-control'}))
