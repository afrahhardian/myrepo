from django.test import TestCase, Client
from django.urls import resolve
from .views import *



class thisIsTest(TestCase):

    def test_hompage_url_is_exsist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_hompage_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Afrahsapp/myWbsite.html')

    def test_homepage_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_about_url_is_exsist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)

    def test_about_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'Afrahsapp/about.html')

    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_contact_url_is_exsist(self):
        response = Client().get('/contact/')
        self.assertEqual(response.status_code,200)

    def test_contact_using_contact_template(self):
        response = Client().get('/contact/')
        self.assertTemplateUsed(response, 'Afrahsapp/contact.html')

    def test_contact_using_contact_func(self):
        found = resolve('/contact/')
        self.assertEqual(found.func, contact)

    def test_photos_url_is_exsist(self):
        response = Client().get('/photos/')
        self.assertEqual(response.status_code,200)

    def test_photos_using_photos_template(self):
        response = Client().get('/photos/')
        self.assertTemplateUsed(response, 'Afrahsapp/photos.html')

    def test_photos_using_photos_func(self):
        found = resolve('/photos/')
        self.assertEqual(found.func, photos)

    def test_form_url_is_exsist(self):
        response = Client().get('/form/')
        self.assertEqual(response.status_code,200)

    def test_form_using_form_template(self):
        response = Client().get('/form/')
        self.assertTemplateUsed(response, 'Afrahsapp/form.html')

    def test_form_using_form_func(self):
        found = resolve('/form/')
        self.assertEqual(found.func, form)
